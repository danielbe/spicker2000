﻿namespace Spicker2000
{
    partial class FormMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.Refresh = new System.Windows.Forms.Button();
            this.Add = new System.Windows.Forms.Button();
            this.Options = new System.Windows.Forms.ComboBox();
            this.DisplayNotiz = new System.Windows.Forms.Button();
            this.NotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // Refresh
            // 
            this.Refresh.Location = new System.Drawing.Point(12, 42);
            this.Refresh.Name = "Refresh";
            this.Refresh.Size = new System.Drawing.Size(92, 23);
            this.Refresh.TabIndex = 2;
            this.Refresh.Text = "Refresh";
            this.Refresh.UseVisualStyleBackColor = true;
            this.Refresh.Click += new System.EventHandler(this.Refresh_Click);
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(110, 42);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 23);
            this.Add.TabIndex = 3;
            this.Add.Text = "Add";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Options
            // 
            this.Options.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Options.FormattingEnabled = true;
            this.Options.Location = new System.Drawing.Point(12, 12);
            this.Options.MaxDropDownItems = 100;
            this.Options.Name = "Options";
            this.Options.Size = new System.Drawing.Size(166, 21);
            this.Options.TabIndex = 0;
            // 
            // DisplayNotiz
            // 
            this.DisplayNotiz.Location = new System.Drawing.Point(184, 10);
            this.DisplayNotiz.Name = "DisplayNotiz";
            this.DisplayNotiz.Size = new System.Drawing.Size(75, 23);
            this.DisplayNotiz.TabIndex = 1;
            this.DisplayNotiz.Text = "Display";
            this.DisplayNotiz.UseVisualStyleBackColor = true;
            this.DisplayNotiz.Click += new System.EventHandler(this.DisplayNotiz_Click);
            // 
            // NotifyIcon
            // 
            this.NotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("NotifyIcon.Icon")));
            this.NotifyIcon.Text = "Spicker2000";
            this.NotifyIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon_MouseClick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 83);
            this.Controls.Add(this.DisplayNotiz);
            this.Controls.Add(this.Options);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.Refresh);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FormMain_KeyPress);
            this.Resize += new System.EventHandler(this.FormMain_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Refresh;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.ComboBox Options;
        private System.Windows.Forms.Button DisplayNotiz;
        private System.Windows.Forms.NotifyIcon NotifyIcon;
    }
}

