﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Spicker2000
{
    static class ReadWriteFile
    {
        public static void CreateFileIfNotExistent()
        {
            //using (StreamWriter w = File.AppendText(Values.FileWithNotizen))
            StreamWriter w = File.AppendText(Values.FileWithNotizen);
        }

        public static void ReadFromFileWriteInNotizListe()
        {
            XmlConfiguration xmlConfiguration = new XmlConfiguration();

            try
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(XmlConfiguration));
                StreamReader reader = new StreamReader(Values.FileWithNotizen);
                xmlConfiguration = (XmlConfiguration)deserializer.Deserialize(reader);
                Values.NotizListe = xmlConfiguration.NotizList;
                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error while reading file");
                //option to create example file
                CreateExampleXmlFile();
            }
        }

        public static void WriteToFile()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(XmlConfiguration));

            XmlConfiguration xmlConfiguration = new XmlConfiguration
            {
                NotizList = Values.NotizListe
            };

            try
            {
                TextWriter writer = new StreamWriter(Values.FileWithNotizen);
                serializer.Serialize(writer, xmlConfiguration);
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error while writing to file");
            }
        }

        private static void CreateExampleXmlFile()
        {
            DialogResult dialogResult = MessageBox.Show("Create an example xml file?",
                "Error while reading file", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Values.NotizListe.Add(new XmlNotiz { Id = 1, Text = "Text", Title = "Title" });
                WriteToFile();
            }
        }
    }
}
