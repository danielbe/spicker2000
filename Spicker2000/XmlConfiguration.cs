﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Spicker2000
{
    [XmlRoot("Configuration")]
    public class XmlConfiguration
    {
        public XmlConfiguration() { }

        [XmlElement("Notiz")]
        public List<XmlNotiz> NotizList { get; set; }
    }

    public class XmlNotiz
    {
        //Id zu XmlAttribute umändern

        [XmlElement("Title")]
        public string Title { get; set; }

        [XmlElement("Text")]
        public string Text { get; set; }

        [XmlElement("Id")]
        public int Id { get; set; }

        //unused
        public void DisplayNotiz(object sender, EventArgs e)
        {
            FormNotiz formSpicker = new FormNotiz(Text, Title, Id);
            formSpicker.Show();
        }

        public override string ToString()
        {
            return $"{Id}_{Title}";
        }
    }
}
