﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spicker2000
{
    public partial class FormNotiz : Form
    {
        public delegate void Delegator();
        public event Delegator DelegatorEvent;

        public new string Text { get; set; }
        public new string Title { get; set; }
        public new int Id { get; set; }

        public FormNotiz(XmlNotiz notiz)
        {
            InitializeComponent();

            Text = notiz.Text;
            Title = notiz.Title;
            Id = notiz.Id;
        }

        public FormNotiz(string text, string title, int id)
        {
            InitializeComponent();

            Text = text;
            Title = title;
            Id = id;
        }

        private void Spicker_Load(object sender, EventArgs e)
        {
            TextBox.Text = Text;
            TextBoxTitle.Text = Title;
        }

        private void CloseForm_Click(object sender, EventArgs e)
        {
            //write the changed text into NotizListe
            Values.NotizListe.Find(o => o.Id.Equals(Id)).Text = TextBox.Text;
            Values.NotizListe.Find(o => o.Id.Equals(Id)).Title = TextBoxTitle.Text;
            DelegatorEvent();

            this.Close();
        }

        private void DeleteNotiz_Click(object sender, EventArgs e)
        {
            Values.NotizListe.Remove(Values.NotizListe.Find(o => o.Id.Equals(Id)));
            DelegatorEvent();

            this.Close();
        }

        bool mouseDown;
        Point lastLocation;

        private void Move_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void Move_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                Location = new Point(
                    (Location.X - lastLocation.X) + e.X, (Location.Y - lastLocation.Y) + e.Y);

                Update();
            }
        }

        private void Move_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
    }
}
