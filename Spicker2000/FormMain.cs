﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Spicker2000
{
    public partial class FormMain : Form
    {

        public FormMain()
        {
            InitializeComponent();
            //notizListe.Add(new XmlNotiz { Title = "Test", Text = "Dies ist ein Beispielstext", Id = 1 });
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            ReadWriteFile.ReadFromFileWriteInNotizListe();
            Values.NotizListe = Values.NotizListe.OrderBy(o => o.Id).ToList();

            AddNotizListeToDropdown();
        }

        private void AddNotizListeToDropdown()
        {
            Options.Items.Clear();

            Values.NotizListe.ForEach(n => Options.Items.Add(n));

            //foreach (XmlNotiz notiz in Values.NotizListe)
            //{
            //    //SuspendLayout();
            //    Options.Items.Add(notiz);
            //}
            Options.Text = "";
        }
        
        private void FormMain_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Soll Einstellungen öffnen
            //- Pfad ändern, wo Notizen gespeichert sind
            //- Aus Xml-Datei lesen?
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            
            ReadWriteFile.WriteToFile();
            Console.WriteLine(@"! Program was closed via the exit button. 
                Every change was saved in the XML file !");
        }

        public void RefreshNotizen()
        {
            ReadWriteFile.WriteToFile();
            AddNotizListeToDropdown();
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            AddNotizListeToDropdown();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if (Options.Items.Count >= Options.MaxDropDownItems)
            {
                MessageBox.Show("You need to remove elements before adding new ones");
            }
            else
            {
                XmlNotiz newNotiz = new XmlNotiz
                {
                    Id = Helper.GenerateNewId(),
                    Text = "",
                    Title = "Neue Notiz"
                };
                Values.NotizListe.Add(newNotiz);
                RefreshNotizen();
            }
        }

        private void DisplayNotiz_Click(object sender, EventArgs e)
        {
            if (!Options.Text.Equals(""))
            {
                FormNotiz formNotiz = new FormNotiz((XmlNotiz)Options.SelectedItem);
                formNotiz.Show();
                formNotiz.DelegatorEvent += RefreshNotizen;
            }
        }

        private void FormMain_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
                NotifyIcon.Visible = true;
            }
        }

        private void NotifyIcon_MouseClick(object sender, MouseEventArgs e)
        {
            ShowForm();
        }

        private void ShowForm()
        {
            Show();
            WindowState = FormWindowState.Normal;
            NotifyIcon.Visible = false;
        }
    }
}
