﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spicker2000
{
    public static class Helper
    {
        public static string ShortenString(string toShorten)
        {
            int maxLength = 20;
            toShorten = "Button" + toShorten;
            if (toShorten.Length > maxLength)
            {
                toShorten = toShorten.Substring(0, maxLength);
                Console.WriteLine($"! Name of button was shortened to {toShorten} !");
            }

            return toShorten;
        }
        
        public static int GenerateNewId()
        {
            int ret = 0;
            int value = Values.NotizListe.Min(n => n.Id);
            if (value > 1)
                ret = value - 1;
            else
                ret = Values.NotizListe.Max(n => n.Id) + 1;
            return ret;
        }
    }
}
