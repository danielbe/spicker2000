﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spicker2000
{
    public static class Values
    {
        public static string FileWithNotizen { get; set; } =
            Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            + @"\notes.xml";

        public static List<XmlNotiz> NotizListe = new List<XmlNotiz>();
        public static List<XmlNotiz> RemoveNotiz = new List<XmlNotiz>();
    }
}
