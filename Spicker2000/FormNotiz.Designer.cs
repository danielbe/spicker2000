﻿namespace Spicker2000
{
    partial class FormNotiz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBox = new System.Windows.Forms.RichTextBox();
            this.CloseForm = new System.Windows.Forms.Button();
            this.DeleteNotiz = new System.Windows.Forms.Button();
            this.TextBoxTitle = new System.Windows.Forms.TextBox();
            this.Move = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBox
            // 
            this.TextBox.Location = new System.Drawing.Point(0, 12);
            this.TextBox.Name = "TextBox";
            this.TextBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.TextBox.Size = new System.Drawing.Size(211, 209);
            this.TextBox.TabIndex = 1;
            this.TextBox.Text = "";
            // 
            // CloseForm
            // 
            this.CloseForm.BackColor = System.Drawing.Color.Red;
            this.CloseForm.ForeColor = System.Drawing.Color.White;
            this.CloseForm.Location = new System.Drawing.Point(188, -4);
            this.CloseForm.Name = "CloseForm";
            this.CloseForm.Size = new System.Drawing.Size(23, 23);
            this.CloseForm.TabIndex = 13;
            this.CloseForm.TabStop = false;
            this.CloseForm.Text = "x";
            this.CloseForm.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.CloseForm.UseVisualStyleBackColor = false;
            this.CloseForm.Click += new System.EventHandler(this.CloseForm_Click);
            // 
            // DeleteNotiz
            // 
            this.DeleteNotiz.Location = new System.Drawing.Point(188, 22);
            this.DeleteNotiz.Name = "DeleteNotiz";
            this.DeleteNotiz.Size = new System.Drawing.Size(23, 23);
            this.DeleteNotiz.TabIndex = 4;
            this.DeleteNotiz.TabStop = false;
            this.DeleteNotiz.Text = "d";
            this.DeleteNotiz.UseVisualStyleBackColor = true;
            this.DeleteNotiz.Click += new System.EventHandler(this.DeleteNotiz_Click);
            // 
            // TextBoxTitle
            // 
            this.TextBoxTitle.Location = new System.Drawing.Point(0, -4);
            this.TextBoxTitle.Name = "TextBoxTitle";
            this.TextBoxTitle.Size = new System.Drawing.Size(211, 20);
            this.TextBoxTitle.TabIndex = 0;
            // 
            // Move
            // 
            this.Move.Location = new System.Drawing.Point(188, 51);
            this.Move.Name = "Move";
            this.Move.Size = new System.Drawing.Size(23, 23);
            this.Move.TabIndex = 14;
            this.Move.TabStop = false;
            this.Move.Text = "m";
            this.Move.UseVisualStyleBackColor = true;
            this.Move.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Move_MouseDown);
            this.Move.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Move_MouseMove);
            this.Move.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Move_MouseUp);
            // 
            // FormNotiz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(211, 221);
            this.Controls.Add(this.Move);
            this.Controls.Add(this.CloseForm);
            this.Controls.Add(this.DeleteNotiz);
            this.Controls.Add(this.TextBoxTitle);
            this.Controls.Add(this.TextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormNotiz";
            this.Text = "Spicker";
            this.Load += new System.EventHandler(this.Spicker_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox TextBox;
        private System.Windows.Forms.Button CloseForm;
        private System.Windows.Forms.Button DeleteNotiz;
        private System.Windows.Forms.TextBox TextBoxTitle;
        private System.Windows.Forms.Button Move;
    }
}